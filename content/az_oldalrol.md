+++
title = "Az oldalról"
path = "/az-oldalrol"
weight = 10
date = "2021-01-30"

[taxonomies]
categories = []
tags = []

[extra]
no_link = true
no_read_more = true
+++

Ez az oldal lelkes amatőrök munkája, a betegséget legyőző vagy még vele küzdő gazdik valódi
tapasztalatain alapul. Célunk az, hogy ez a még kísérleti, de hatásos gyógymód ismertté váljon
hazánkban is. A kezelés annyira új, hogy sok állatorvos sem hallott róla, vagy szkeptikus a
hatékonyságát illetően. Az oldalon leírtakat látta állatorvos, de a jogi helyzet kétségessége miatt
csak név nélkül vállalta a lektorálást. Az itt összeszedett tudásanyag javarészt a
[fiptreatment.com][1] oldal szabados fordítása, kiegészítve néhány javítással, a magyar közösség
tapasztalataival és egyéb forrásokból gyűjtött információkkal.

<center><em>Azokért a társainkért, akik már átkeltek a szivárványhídon.</em></center>

<!-- more -->

[1]: https://fiptreatment.com/
