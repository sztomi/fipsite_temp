+++
title = "Mi az a FIP?"
path = "/mi-a-fip"
weight = 0
date = "2021-01-30"

[taxonomies]
categories = []
tags = []

[extra]
no_link = true
no_read_more = true
+++

A FIP-et (*Feline Infectious Peritonitis*), azaz a *macskák fertőző hashártyagyulladását* a jelenleg
domináns nézetek szerint az egyébként javarészt ártalmatlan FeCV (*Feline Enteric Coronavirus*)
mutációja váltja ki. Többnyire két évesnél fiatalabb vagy legyengült immunrendszerű macskákat támad
meg. A vírus hatására az immunrendszer túlpörög és az egész szervezet heves gyulladásba jön. A
betegség nevével ellentétben nem csak a hashártyán okozhat léziókat és autoimmun tüneteket, hanem a
szervezet több pontján (pl. vese, máj károsodása).

2019 februárjában Dr. Niels Pedersen (UC Davis) és kutatótársai publikálták [tanulmányukat][1],
melyben óriási áttörést ismertettek: egy hatásos antivirális szerrel **gyógyíthatóvá vált** az eddig
halálos ítélettel egyenlő FIP.

A macskák túlnyomó többsége találkozik az enteriális koronavírussal (FeCV), de ezt különösebb
komplikációk nélkül átvészelik (többnyire némi hasmenéssel jár). Kis százalékukban azonban, jelenleg
ismeretlen okokból (legyengült immunrendszer vagy genetikai tényezők miatt) kialakul a FIP.

<!-- more -->

[1]: https://pubmed.ncbi.nlm.nih.gov/30755068/