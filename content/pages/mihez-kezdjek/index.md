+++
title = "Most mihez kezdjek?"
path = "mihez-kezdjek"
template = "about.html"

[extra]
cover = "olga-salova-w-VWKVB0P_U-unsplash"
#subheading = "Hogyan diagnosztizálható a FIP?"
+++

Ha úgy találtál erre az oldalra, mint legtöbben, akkor valószínűleg van egy FIP-es vagy FIP-gyanús
macskád. Rákerestél a betegségre, és lépten-nyomon azt találtad, hogy gyógyíthatatlan. Talán még az
állatorvosod is ezt mondta, és mielőbbi altatást javasolt. Erre itt van ez az oldal, talán hiszed
is, meg nem is, hogy mégis van gyógymód. Mihez kezdj egy ilyen lehetetlen helyzetben? A sok-sok
életmentő, de száraz információ mellett szeretnénk egy kis lelki támogatást is nyújtani.

Mindannyian, akik ebbe belecsöppenünk, hasonló dolgokon megyünk keresztül, hasonló kétségeink
vannak, és mind ugyanabba a reménysugárba kapaszkodunk. Ha mindez néhány évvel ezelőtt történik
veled és a cicáddal, akkor egy sokkal rövidebb, de tragikus esemény lenne: a cica meghal egy
borzalmas, gyógyíthatatlan betegségben. Ma egy határvonalon vagyunk: itt a gyógyszer, a felépülés
lehetségessé vált. De nincs törzskönyvezve, nehéz és drága beszerezni, és a teljes kezelés óriási
próbatétel, nem csak a cicának, hanem a gazdinak is. Normális, ha félsz, hogy felesleges szenvedést
okozol a cicádnak a kezeléssel. De fontos tudni, hogy csak néhány másodpercnyi kellemetlenség a
számukra az injekció, utána boldogan teszik a dolgukat, semmiféle tartós életminőség-romlást nem
okoz a kezelés. Sok állat él cukorbetegséggel vagy veseproblémákkal, az ő esetükben az injekció vagy
infúzió mindennapos feladat az állata élete végéig. Ebben az esetben viszont csak 84 napra szól.

A kezelést elkezdők leggyakoribb tapasztalata, hogy a javulás nagyon gyorsan bekövetkezik. Az első
néhány nap után egyre jobb lesz a cicád hangulata, esetleg többet is eszik majd, játékosabb,
mozgékonyabb lesz, néhány hét alatt a leletei is javulni fognak, és ha folyadék volt a
szervezetében, az is fokozatosan felszívódik, vagy a neurológiai tünetek szűnnek meg. Ez minden nap
örömteli felfedezésekkel jár. Sokszor már nem is tudjuk, hogy mennyivel kevésbé energikus volt a
cicánk, csak vesszük észre, amikor a kezelés hatására sokkal jobban lesz.

Ami az elején nagyon hosszúnak, félelmetesnek, reménytelennek látszik, az a kezelés során először
rutintevékenységgé változik. Aztán amint látni a végét a folyamatnak, onnantól a gazdik arra szoktak
koncentrálni, hogy már csak egy keveset kell kibírni, a kezelés vége pedig egy igazi ünnep mind a
cicának, mind a gazdinak.

Rettenetes, hogy egy ilyen betegséggel kell megküzdened a cicával együtt, de ha belevágsz a
kezelésbe, nagy az esély rá, hogy sikerülni fog. Esélyt adsz az életre a cicádnak, ami a diagnózis
pillanatában még lehetetlennek tűnt.

Itthon többszáz, nemzetközileg többezer, talán tízezer cica a bizonyíték rá, hogy igenis
lehetséges.

A kezelés nagyon sokba kerül. Pár év múlva, amikor már törzskönyvezve lesz a gyógyszer, talán már
más lesz a helyzet, de ez most még óriási teher mindenkinek. Talán akkora, hogy nem is látod most
megoldhatónak. Tudnod kell, nem egyszerre kell kifizetni a kezelés árát, menet közben is megveheted
folyamatosan. Ezzel nem vagy egyedül. Kérj segítséget, rendezz gyűjtést, ne hagyd, hogy anyagi
okokból meg kelljen halnia a társadnak. Ha attól félsz, hogy nem tudod összegyűjteni a kezelés árát,
akkor is érdemes belekezdeni: ha a szer működik, akkor lesz rá időd, hogy pénzt gyűjts.

Tehát mihez kezdj? Az első dolgod, hogy bízz a gyógyulásban. A második, hogy lépj be a [csoportba][1],
írd le röviden a helyzetedet. Szerezd be az első adag gyógyszert még ma, és kezdd el a kezelést.

Óriási küzdelem ez, de felemelő is: egyenesen a halálnak mondasz nemet.

<a href="https://www.facebook.com/groups/1059378271077244" role="button" class="btn btn-primary" style="text-decoration: none"><i class="fab fa-facebook-square"></i> Facebook csoport</a>

[1]: https://www.facebook.com/groups/1059378271077244