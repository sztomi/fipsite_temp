+++
title = "Gyakran Ismételt Kérdések"
path = "/gyik"
template = "about.html"

[extra]
cover = "132830195_3505230206212028_5400373821998975642_o"
subheading = ""
+++

#### Van-e védőoltás a FIP ellen? {#oltas}

Egy oltás létezett, azonban ennek a megbízhatósága nagyon alacsony volt (erősen vitatott, hogy
egyáltalán működött-e). Emiatt nem is lehet már beszerezni, és a legtöbb állatorvos sem
ajánlja.


#### Létezik teszt vagy módszer, ami biztosan kimutatja a FIP fertőzést? {#teszt}

Magát a FIP vírust kimutatni nagyon nehéz. Mivel a FIP a jobbára ártalmatlan FeCV (Feline enteric
Coronavirus) mutálódott változata, ezért mind az aktív fertőzést kimutatni képes PCR teszt, mind az
immunválaszt (antitesteket) kimutató ELISA teszt egyformán jelez, és nem lehet köztük különbséget
tenni. Viszont a FeCV önmagában nem, vagy csak minimális kellemetlenséget okoz. A FIP diagnózisa
mindig az összes tünet, ultrahang, vérkép együttes figyelembevételével történik. További információk
a [Diagnózis](/diagnozis) oldalon.


#### Miért nem alkalmas a PCR-teszt a FIP kimutatására? {#pcr}

Ennek több oka van:

  * A PCR-teszt nem tud különbséget tenni a jobbára ártalmatlan FeCV (enteriális koronavírus) és FIP között
  * Függetlenül attól, hogy székletből, hasűri folyadékból vagy vérből végzik, a vírus nem lesz
    biztosan jelen még aktív FIP-fertőzés mellett sem!

Tehát ha végzünk egy (vagy több) PCR-tesztet:

  * **Pozitív** teszt esetén az alábbi lehetőségek vannak, amik között nincs módunk kiválasztani a
    a tényleges okot:
    * Lehet FIP
    * Lehet FeCV
    * Lehet hamis-pozitív

  * **Negatív** teszt esetén:
    * Lehet, hogy csak a vizsgált mintában nem volt vírus (akár többször is), miközben a fertőzés
      jelen van
    * Lehet hamis-negatív

{% warning(title="NE végezzünk PCR-tesztet") %}
Ahogy a fentiekből látható, a PCR-teszt <em>egyáltalán nem fog segíteni a diagnózis felállításában</em>.
Viszont általában külső laborba kell küldeni a mintát, és napokig-hetekig tart, amíg megérkezik
az eredmény. A FIP-kezelés sikeressége szempontjából az egyik legfontosabb tényező az idő. Tehát
ne fecséreljük el egy ilyen tesztre, helyette készüljön teljes vérkép, biokémia, ultrahang, esetleg
röntgen. Bővebben a <a href="/diagnozis">Diagnózis</a> oldalon.
{% end %}


#### Az állatorvosom azt mondta, hogy altatni kell a macskámat a FIP miatt, honnan veszitek, hogy gyógyítható? Ez nem csak valami lehúzás? {#nem-lehuzas}

Ez az oldal azért jött létre, mert az említett kezelés és antivirális szer még nem ismert széles
körben, sok állatorvos sem hallott róla. Nem árulunk és nem reklámozunk semmit itt és a [FIP
Magyarország][1] csoport sem teszi (de segít a beszerzésben). Az oldal célja az, hogy ezt a kezelést
ismertebbé tegyük és hogy ne az altatás legyen az első és egyetlen lépés egy ilyen diagnózis után, hanem egy
informált döntést hozhasson mindenki, és esetleg próbálja meg ezt a kísérleti, de eddig láthatóan
nagyon eredményes kezelést.

[Dr. Niels Pedersen 2019-es kutatása][2] klinikai körülmények között 31-ből 25 macskát sikerrel
gyógyított. Már a kutatás megjelenése előtt is, és azóta is világszerte nagyon sok macska meggyógyult ennek a
kezelésnek köszönhetően. A tapasztalat azt mutatja, hogy az időben elkezdett kezelés
kulcsfontosságú.


#### Milyen kezeléssel gyógyítható a FIP? {#kezeles}

A FIP kezelésére használt antivirális szer neve [GS-441524][3]. Bővebben a [Gyógyítás](/gyogyitas)
oldalon.

#### Hogyan találom meg a facebook csoportot? {#facebook}

A csoport neve [FIP Magyarország][1]. Ha esetleg nem található a linken, akkor keress rá erre,
illetve érdeklődj macskás csoportokban, esetleg nagyobb menhelyeknél.

#### Mire jó a facebook csoport? {#facebook-miert}

A csoport tagjai korábbi és jelenlegi FIP-es macskák gazdijai, mindenki hasonló élményeken ment keresztül.
Itt lehet gyakorlati kérdéseket feltenni, érzelmi támogatást adni és kapni. A gyógyszer
beszerzésében is tud segíteni a csoport, ami főként a folyamat elején nagyon fontos lehet, hogy
gyorsan el lehessen kezdeni a kezelést.


#### Mi köze a Remdesivirnek a GS-441524-hez? {#remdesivir}

A Remdesivir, amiről a koronavírus-járvány kapcsán hallhattál, egy nagyon hasonló szer (kódja:
GS-5734). Ez a molekula a GS-441524 kis mértékben módosított változata, ami a sejtfalon való
átjutását segíti a szernek, de a hatásmechanizmusát elvileg nem változtatja meg. Az eredeti
kutatásban mindkét molekulát vizsgálták in vitro, és azt találták, hogy a GS-441524 ugyanannyira
hatékony, viszont egyszerűbben, olcsóbban állítható elő. Tanulmányok jelenleg a GS-441524-ről
szólnak, és a magyar és külföldi csoportok tapasztalati is erre vontakoznak. Ugyanakkor már lehet
hallani arról, hogy sikerrel kezeltek Remdesivirrel is macskákat. Ez azért fontos, mert jelenleg a
Remdesivir már rendelkezik törzskönnyvel ([hazánkban is][7], de kiadhatósága csak intézményi), ami
elengedhetetlen ahhoz, hogy később bevett módon használható legyen az állatgyógyászatban is. Dr
Pedersen [írja][8]:

> Groups in Australia and some Asian countries are starting to use it and report identical results
> to GS-441524. The dosage of Remdesivir on a molar basis is theoretically the same as GS-441524.
> GS-441524 has a molecular weight of 291.3 g/M, while Remdesivir is 442.3 g/M. Therefore, it would
> take 442.3/291.3=1.5 mg of Remdesivir to yield 1 mg of GS-441524. The diluent for Remdesivir is
> significantly different than the diluent used for GS-441524 and designed for IV use in humans. How
> diluted Remdesivir will behave when injected subcutaneously over 12 or more weeks of daily
> treatment is not known. Finally, mild signs of both liver and kidney toxicity have been seen with
> Remdesivir in humans. GS-441524 causes mild and non-progressive renal toxicity in cats but with no
> apparent liver toxicity. It is uncertain whether the renal toxicity seen in humans given
> Remdesivir is due to its active ingredient (i.e., GS-441524) or to the chemical additions meant to
> enhance antiviral activity.

Tehát jelenleg, bár az első tapasztalatok bíztatóak, még igencsak gyerekcipőben jár a Remdesivir
alkalmazása, illetve vannak kérdések a mellékhatásokat illetően. Jelenleg a kezelésre a legjobb
opció továbbra is a GS-441524.

#### Hallottam erről a kezelésről egy másik csoportban, azt mondták, hogy csak kínzás a macskának. Miért nektek higgyek? {#nem-kinzas}

Egy korábban gyógyíthatatlan betegségről van szó, ezért esetleges érintettség esetén komoly érzelmi
alapú érvekkel találkozhatunk. Ha néhány évvel ezelőtt valaki elvesztette a macskáját emiatt,
természetes, hogy hitetlenkedni fog. Ha esetleg nem is évekkel ezelőtt történt, hanem egyszerűen
csak nem értesült a gyógymódról (vagy esetleg anyagi korlátai voltak), akkor sokan még inkább
hajlamosak lesznek tagadni a valóságot. Azt a valóságot, amit [tudományos kutatások][2] és hús-vér
emberek tapasztalatai támasztanak alá. Ha kétségeid vannak, lépj be a [FIP Magyarország
csoportba][1], és kérdezz bátran. Itt, az oldalon is elkezdtük gyűjteni a [túlélők történeteit][6].


#### Miből áll a kezelés? {#kezeles-menete}

A GS egy bőr alá adott injekcó, amit 12 héten, azaz 84 napon keresztül naponta kell beadni. Ezt
követi 12 hét megfigyelési időszak, ami után a macska gyógyultnak nyilvánítható. Elképzelhető
visszaesés, ilyenkor nagyobb dózissal egy rövidebb injekciós kezelést szoktak alkalmazni.
Bővebben a [Gyógyítás](/gyogyitas) oldalon.

#### Mennyibe kerül? {#mennyibe-kerul}

A GS hozzánk tipikusan 5ml-es üvegcsékben érkezik, amiknek darabonként (az írás pillanatában) kb. 75
USD az ára, ami kb. 22-25e Ft, de a különböző gyártók termékei mind árban, mind kiszerelésben eltérhetnek az itt említettől.
Egy dózis a macska tömegétől és állapotától függően változik, és az
állatorvos dönti el, a kezelés közben növekedhet is. Egy kb 2,5kg-os macska dózisa, neurológiai
tünetek nélkül, 1ml, 24 óránként (de ezt csak szemléltetés miatt írjuk, a dózist minden esetben
egyedileg állapítják meg). Nem ritka, hogy a kezelés közben változik a dózis.

Tehát összesen 200 000 Ft-tól akár egymillió Ft-ig is terjedhet az összeg. A legtöbb esetben kb.
300-500 ezer Ft között mozog a GS, a kezelés költsége természetesen több is lehet a szükséges diagnosztika és egyéb költségek függvényében.

Ez az összeg a teljes, 84 napos kezelésre vetítve elsőre ijesztő lehet, de fontos tudni, hogy ezt
nem egyszerre kell kifizetni. Mindenképp keresd fel a [csoportot][1] akkor is, ha a kezelést csak
anyagi okokból nem látod megoldhatónak. A csoportban sokan vannak, akik sikerrel szerveztek gyűjtést
barátaik, ismerőseik között. Van remény, ne hagyd, hogy ez eltántorítson.

#### Az állatorvosom szerint egy 30 napos / egy hetes kezelés elegendő, miért folytassam 84 napig? {#protokoll}

A GS a vírus szaporodását gátolja. Ez azt jelenti, hogy az idő előrehaladtával (a sejtek
pusztulásával és újratermelődésével) egyre kevesebb vírusos sejt marad a szervezetben. Az
immunrendszer fel tud lélegezni, a gyulladások megszűnnek, esetleges bakteriális fertőzések
meggyógyulnak. Elképzelhető, hogy bizonyos esetekben elegendő lenne a 30 nap, de jelenleg még nincs
biztos eszközünk arra, hogy ezt meg lehessen állapítani. Bár rendszerint nagyon gyorsan, látványosan
jobban vannak a cicák a kezeléstől, ez csalóka, mert sokáig jelen lehet a vírus a szervezetükben,
ami a kezelés megszüntetése után újra erőre kaphat. Jelenleg a kezelés egy nagyon korai eredményen
alapul és ez a 84 napos protokoll az, amiről tudjuk, hogy nagy eséllyel működik - minden más csak
kísérletezés, az állat életének kockára tételével. Ahogy a kutatások haladnak, pár év múlva már talán
egy sokkal gyorsabb protokoll lesz, más, hasonló szerekre alapozva - de sajnos még nem tartunk ott.

#### Az állatorvosom interferonnal/szteroiddal/stb. való kezelést javasolja, miért ne azt válasszam? {#interferon-szteroid}

Ezek nem gyógymódok, csak a tünetek kezelésére szolgálnak, leginkább csak időt nyerhetnek az altatás
előtt. A GS alkalmazása előtt ezek voltak a bevett kezelési módok FIP esetén, de nem jelentettek
gyógymódot. A GS kezelés általában gyorsan (néhány nap alatt) látható javulást eredményez.

#### Beadhatom magam a GS-t a macskámnak? {#otthoni-beadas}

Igen, ha bízol magadban, ez egy viszonylag könnyen elsajátítható művelet. Bőr alá adott injekció,
tehát a legkönnyebb típus. Kérd az állatorvosodat, hogy tanítsa meg a beadást, illetve Youtube
videók segíthetnek. Vannak olyan gazdik, akik nagyon messze élnek minden állatorvostól, aki vállalná
a kezelést, és első perctől maguk adják be az injekciókat.

A fecskendőbe való felszívás után fontos tűt cserélni, mert a tű csorbul, valamint a GS vivőanyaga
erősen savas, emiatt a felszínásra használt tű felülete már csíphet.

Fontos az állat megfelelő lefogása, stabilan tartása, hogy a legkisebb kellemetlenség mellett
a legnagyobb biztonsággal beadható legyen a szer, ehhez néha egynél több emberre is szükség lehet.

#### Tablettában elérhető a GS? {#tabletta}

Igen, létezik tabletta formátumú GS (pl. Mutian X, Lucky). Ezekről a magyar csoportban kevesebb
tapasztalat áll rendelkezésre, és a csoporton keresztül nem is lehet beszerezni. Külföldi
csoportokban ezzel is eredményesen kezelik a FIP-es macskákat.

Általánosságban kijelenthető viszont, hogy az injekciós kezelés megbízhatóbb felszívódással
rendelkezik és valószínűleg összességében kevesebb szenvedést is okoz a macskáknak (ez elsőre
furcsán hangozhat, de egy tabletta beadása jóval nagyobb stresszt és kényszerítést jelenthet, és
több ideig is tart).

Nagy dózist igénylő eseteknél szinte biztos, hogy nem választható a tabletta, mert nem tud elég
GS felszívódni belőle.


#### Kell-e még más gyógyszert adni a macskámnak a kezelés alatt? {#kiegeszito-kezeles}

Ezt az állatorvosoddal kell egyeztetned, mert függ a cica állapotától és esetleges egyéb
betegségeitől. Gyakori, hogy májvédő, B12 és antibiotikumok szükségesek, leginkább a kezelés elején.
Calopet, epato paszta sokszor ajánlott, valamint Vetri DMG (bár az utóbbiról megoszlanak a
vélemények még a csoportban is - az állatorvosod véleménye a mérvadó a te cicád esetében). Néha
vízhajtót is kapnak, főleg, ha a mellkasukat érinti a vizesedés nedves FIP esetén. De a folyadék
nagy része a kezelés során, magától fog felszívódni (ami akár heteket is igénybe vehet).


#### Ha megrendelem a szert, mennyi idő lesz, mire kézhez kapom és elkezdhetem a kezelést? {#beszerzes-ido}

Ez változó, attól függ, hogy akitől rendelted, mennyi idő alatt tudja szállítani neked a gyógyszert.
Viszont nem kell ezt kivárnod: gyakori, hogy az új tagok a csoportba belépve rögtön a többiektől
vesznek vagy kölcsön kapnak egy-két üvegcsét, hogy azonnal elkezdhessék a kezelést. Ha eldöntötted,
hogy belevágsz, ez legyen az első lépésed, így tudsz a leggyorsabban hozzájutni.


#### Hogyan kaphatta el a macskám a betegséget, ha a lakásban tartottam és nem érintkezett más macskákkal? {#hogyan-kapta-el}

Jelenlegi ismereteink szerint a FIP vírust közvetlenül ritkán szokták továbbadni a macskák
egymásnak. Amit továbbadnak, az az FeCV (Feline Enteric Coronavirus), a macskák koronavírusa. Ez a
fertőzés viszont önmagában ritkán hordoz nagy kockázatot, leginkább csak hasmenést szokott okozoni
(ha egyáltalán okoz bármi észrevehető tünetet). Ezt sok macska elkapja, tenyészetekben, menhelyeken és többmacskás
háztartásokban különösen. Miután a macska megfertőződött az FeCV-vel, a szervezetén belül mutálódik
a vírus, és így alakul ki a FIP fertőzés. Lehet, hogy hosszú ideig hordozza a koronavírust a macska,
és majd csak később lesz FIP-es. A FIP-pel fertőzött macskák is jellemzően csak az FeCV-t adják
tovább, például székleten keresztül.


#### Oltás, bódítás, ivartalanítás stb. okozhatta a FIP kialakulását a macskámnál? {#oltasok}

A magyar és a nemzetközi csoportokban is nagyon sok anekdotikus történetet lehet arról hallani, hogy
például a kombioltás vagy az ivartalanítás “okozza” a FIP-et. Szögezzük le: nem tudjuk biztosan,
hogy mi okozza a jobbára ártalmatlan FeCV mutálódását FIP-pé. Amit biztosan tudunk, az az, hogy [6
és 36 hónap között][5] a legnagyobb a FIP kialakulásának valószínűsége, ami történetesen egybeesik
ezen beavatkozások leggyakoribb időszakával. Jelenleg semmilyen bizonyíték nincs arra vonatkozóan,
hogy a fentiek bármelyike növelné a FIP kockázatát. Ilyenkor biztosan látja állatorvos a macskát,
ami növeli a valószínűségét, hogy feltűnnek a kezdeti tünetek valakinek. Természetesen nem kizárt,
hogy van összefüggés az oltással járó stressz, legyengült immunrendszer és a FIP kialakulása között,
de erre eddig nincs tudományos bizonyíték.

#### Koronavírus? A macskám covidos lett? {#covid}

A koronavírusok a vírusok egy csoportját jelentik, és nem csak a Covid-19 betegséget okozó
SARS-CoV-2 koronavírust értjük alattuk. Koronavírus okozza még a náthát is például az emberekben,
csak az annyira enyhe lefolyású, hogy nem volt szükség oltást vagy gyógyszert fejleszteni rá. Bár
vannak arra vonatkozóan bizonyítékok, hogy a SARS-CoV-2 is megfertőzhet macskaféléket, a FIP-nek
ehhez jelenlegi ismereteink szerint nincs köze. A FIP vírus egy mutálódott FeCV vírus (így mindkettő
a koronavírusok csoportjába sorolható).


#### Tenyésztő vagyok, hogyan tudom elkerülni, hogy a tenyészetemből kikerült macskák FIP-esek legyenek? {#tenyesztoknek}

Ha tenyésztő vagy, akkor a legjobb dolog, amit tehetsz, hogy a tenyészetet FeCV-mentessé teszed.
Ennek érdekében a következőket teheted:

**Folyamatosan, egyenként több PCR-tesztet végzel az állataidon**: Bár fentebb írtunk arról, hogy a
[PCR-teszt](#pcr) nem segíti a FIP diagnosztizálását, ebben az esetben (többszörös teszteléssel) a
koronavírus jelenléte kizárható.

**Karantén**: Különösen fontos új tenyészállatok bekerülésekor, illetve külső helyszínek (pl.
kiállítás) látogatása után, hogy az érintett állatok többszörös negatív teszteredmény előtt ne
érintkezzenek a többiekkel.

**Immunerősítés és stressz kerülése**: Bár az pontosan nem ismert, hogy a FeCV miért mutálódik FIP
vírussá, az valószínűnek tűnik, hogy legyengült immunrendszer és a stressz gyakran együtt jár vele.

{% danger(title="Ne végezzünk preventív célzattal GS-kezelést!") %}
A GS kezelés hatásossága nem vizsgált a FeCV ellen, valamint óriási tehertétel a macskáknak. Arról
nem is beszélve, hogy a jelenleg beszerezhető gyógyszer alkalmazása rizikós (a beszerzési forrásból
fakadóan), amit csak akkor vállaljunk fel, ha az állat élete a tét.

Mindezek mellett még rezisztencia is kialakulhat, ami egy esetleges jövőbeli FIP-fertőzés
leküzdésében ronthatja drasztikusan a macska esélyeit.
{% end %}

**Mit tegyek, ha a tenyészetemben jelen van a FeCV?**

Ha felelőségteljesen akarsz eljárni, akkor:

  * A FeCV+ macskákat el kell távolítani a tenyészetből
  * A többi állatot többszörösen tesztelni kell
  * A tenyészetet fertőtleníteni kell

A tenyészetből kikerülő FeCV+ macskákat csak a kockázatok ismertetésével szabad el- vagy örökbeadni.
Utánkövetni kell őket, és egy esetleges FIP fertőzés esetén a kezelésük árát finanszírozni.



[1]: https://www.facebook.com/groups/1059378271077244
[2]: https://pubmed.ncbi.nlm.nih.gov/30755068/
[3]: https://en.wikipedia.org/wiki/GS-441524
[4]: https://www.tandfonline.com/doi/full/10.1080/01652176.2020.1845917
[5]: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7150141/
[6]: /tulelok
[7]: https://ogyei.gov.hu/gyogyszeradatbazis&action=show_details&item=200388
[8]: http://sockfip.org/summary-of-gs-441524-treatment-for-fip-2/