+++
title = "Gyógyítás"
path = "gyogyitas"
template = "about.html"

[extra]
cover = "sereja-ris-nnZSvW7A1Ko-unsplash"
subheading = "A FIP gyógyítása és kiegészítő kezelése"
+++

## A FIP gyógyszere: GS-441524

2019 februárja előtt a macskák fertőző hashártyagyulladása (FIP) halálos ítélettel volt egyenlő. Dr.
Niels Pedersen és kutatótársai elhivatott munkájának köszönhetően ma kezünkben van a megoldás: a
[GS-441524][1] nukleinsav-analóg antivirális szer (sokszor csak "GS"-ként hivatkozunk rá, bár ez egy
kissé pontatlan).

{{ fluid_svg(src="/img/GS-441524_skeletal" alt="A GS-441524 molekula struktúrája, az alkotó atomok és a köztük levő kémiai kötések szemléltetésével") }}

<p class="img-caption">A GS-441524 molekula struktúrája. Forrás: <a href="https://en.wikipedia.org/wiki/GS-441524#/media/File:GS-441524_skeletal.svg">Wikipedia</a>.</p>

### Hogyan gyógyít a GS?

Amíg a macska immunrendszere egészséges és kellően fejlett, a fehérvérsejtek képesek leküzdeni a
vírusos fertőzéseket, immunválaszt indítani és antitesteket termelni, amik megvédik az állatot a
betegségtől. Ha az immunrendszer legyengült vagy valamilyen okból hibásan működik, a mutálódott
enteriális koronavírus (FeCV) a makrofágokat, a fehérvérsejtek egy csoportját fertőzi meg. Ekkor az
immunrendszer egyre hevesebb immunválasszal reagál, miközben éppen a vírust terjeszti a szervezet
különböző pontjain. Ha az immunválasz erőteljes, akkor nedves FIP alakul ki, ha visszafogottabb,
akkor száraz FIP.

A GS-441524 egy nukleinsav-analóg vegyület. Ez azt jelenti, hogy a vírus RNS-ébe az egyik nukleinsav
helyére be tud épülni, és meg tudja akadályozni a vírus szaporodását. Ezáltal megszakítja a fenti
láncreakciót, a vírus mennyisége fokozatosan csökken a szerzvezetben, az immunrendszer
fellélegezhet, a gyulladások megszűnnek és a kezelés végére nem marad vírus a szervezetben.

### A kezelés menete

Dr. Pedersen tanulmánya azt állapítja meg, hogy a kezelés menete egy **84 napon** át tartó, naponta
végzett GS-441524 kúra. A 84 nap leteltével újabb 84 nap megfigyelési idő következik. Ha a
megfigyelési idő végén sincsenek tünetek és a laboreredmények is jók, a macska gyógyultnak minősül.
Fontos, hogy a 24 órás intervallumot minél pontosabban betartsuk.

Ez egy hosszú, majdnem három hónapon át tartó kezelés, ami érzelmileg és anyagilag is megterhelő.
Emiatt erős a kísértés, hogy a tünetek elmúlása és a laboreredmények helyreállása után abbahagyjuk a
kezelést, ez azonban tévút lehet: ilyenkor nagy a visszaesés kockázata.

A GS elérhető injekciós és tablettás formában is, azonban a magyar közösségben főleg az injekciós
formáról vannak tapasztalatok. Fontos kiemelni, hogy az injekció felszívódása megbízhatóbb, mint
a tablettában kapott gyógyszereké.

A dózist a macska súlyához és tüneteihez mérten állapítják meg. Az "alap" dózis nedves és száraz
FIP esetén 5mg/kg 24 óránként. Szemtünetekkel (okuláris FIP) a minimum dózis 8mg/kg, neurológiai
tünetek esetén pedig minimum 10mg/kg. Ezek az értékek csak iránymutatásként vannak megadva, a
konkrét dózist minden esetben egyedileg kell megállapítani és akár a kezelés közben növelni is. [Itt
található egy dóziskalkulátor][2].

Fontos a kezelés elején egy teljes vérlabor, biokémia, ultrahang, esetleg röntgen is. Ez a
kiindulási alap, amihez viszonyítani tudunk majd menet közben és a végén, valamint segít
megállapítani a szükséges kiegészítő kezeléseket is. Menet közben, akár 21 naponként a diagnosztika
ismételhető a cica állapotától függően.

{% warning(title="A kezelés hosszáról") %}
<strong>A kezelés 84 napos</strong>, ez az időtartam az,
amire ma <u>tudományos bizonyíték van</u>. Elképzelhető, hogy tovább is folytatni kell, ha nem
állnak helyre a leletek ennyi idő alatt. Minden esetben a 84. nap előtt szükséges egy újabb
labor, hogy ezt az állatorvossal konzultálva el lehessen dönteni.
{% end %}

{% danger(title="Folyadék leszívása") %}
Nedves FIP esetén, különösen mellkasi folyadékot szükséges lehet leszívással eltávolítani. Ezzel
azonban csak nagyon indokolt esetben szabad élni (pl. ha a légzést olyan mértékben akadályozza, hogy
az állat életét fenyegeti). A folyadék nagyon sok fehérjét és ásványi anyagot tartalmaz, amit a
leszívással eltávolítanánk. Ehelyett kíméletesebben, vízhajtókkal (furosemid, verospiron) próbáljuk
meg elérni a kívánt célt. A kezelés előrehaladtával a folyadék magától fel fog szívódni, de erre
akár heteket is várnunk kell. Hasi folyadék esetében kevesebb okunk van vízhatjtók használatára is.
{% end %}

### Kiegészítő kezelés

Bár a GS valóban "csodaszer", önmagában még nem feltétlenül elég a sikeres gyógyuláshoz. A betegség
természete miatt összetett kórkép alakul ki, és a tüneteket külön-külön is gyógyítani kell. Ez a
fajta kiegészítő kezelés döntő lehet a túlélés szempontjából. Mikről lehet szó? A vérszegénységet,
vese -és májproblémákat, valamint esetleges bakteriális felülfertőzéseket kell kezelni. További
információk a kiegészítő kezelésekről [ide kattintva][3] érhetők el.

## Egyéb gyógyszerek (prednizolon / szteroidok)

A prednizolont gyakran írják fel a diagnózis felállításakor, mivel ez egy gyulladáscsökkentő, és
segíthet a macska közérzetének javításában, valamint még az étvágyat is serkenti.

Miután a macska a GS kezelést megkezdte, a prednizolonra és egyéb gyógyszerekre már nincs szükség
(hacsak az állatorvos máshogyan nem rendeli konkrét problémák kezelésére, pl. antibiotikumok).

Ha a GS-kezelés miatt új állatorvosunk van, mindenképpen tájékoztassuk az eddig kapott kezelésekről,
gyógyszerekről, és egyeztessünk azok leállításának menetéről vagy a folytatásukról.

[1]: https://en.wikipedia.org/wiki/GS-441524
[2]: https://fiptreatment.com/dose-calculator/
[3]: /kiegeszito-kezeles