+++
title = "Kiegészítő kezelés"
path = "/kiegeszito-kezeles"
template = "about.html"

[extra]
cover = "abdullah-oguk-MeShkX-rLng-unsplash"
subheading = "Egyéb gyakori szövődmények kezelése"
+++

A FIP egy nagyon veszélyes betegség, és a természetéből fakadóan a szervezet több pontján is
változatos gondokat okozhat. A [GS kezelés mellett][1] ezért elengedhetetlen, hogy ezeket a
problémákat is kezeljük.

Az alábbi információk nem helyettesítik a megfelelő állatorvosi ellátást. Minden esetben
egyeztessünk az állatorvossal a személyre szabott kezelést illetően.

## Vérszegénység (anémia) {#verszegenyseg}

A vérszegénység a FIP-es macskákban nagyon gyakori, olyannyira, hogy ez az egyik klasszikus jel,
amelyet az állatorvos keres a vérképben a diagnózis felállításakor. Ennek legszembetűnőbb indikátora
a sápadt, esetleg szürkés fogíny, szájpadlás (esetleg orr, ha egyébként rózsaszín). Az erősen
vérszegény macskák almot ehetnek, vagy furcsa tárgyakat nyalhatnak, rágcsálhatnak (pica). A
vérszegénység önmagában is életveszélyes lehet, ezért ezt nem szabad figyelmen kívül hagyni.

Az enyhén vérszegény macskák számára előnyös a B12 injekció, amit szükség esetén hetente
vagy akár naponta lehet adni. A fölösleges B12 a vizelettel távozik.

Súlyosan vérszegény macskának (HCT-érték <15%) vérátömlesztésre lehet szüksége. A 10% alatti
HCT-érték vészhelyzet, és mindenképpen transzfúziót igényel.

## Kiszáradás {#kiszaradas}

A FIP gyakran a veséket is megtámadja, ami fokozott vizelést és szomjúságot vált ki.
Folyadékpótlásra lehet szükség, és a víznek mindig a cica számára elérhető helyen kell lennie (akár
a lakás több pontján is).

A macska dehidratáltságának tesztelésére jó módszer az, ha enyhén megszorítjuk a bőrét. Ha a bőr
rugalmas és visszapattan a helyére, a macska megfelelően hidratált. Ha a bőr lassan reagál, akkor
dehidratált.

Enyhe dehidráció esetén elegendő lehet vizet, KMR-t vagy pedialitot fecskendezni a macska szájába.
Vízbevitelüknek minimum napi 60 ml-nek kell lennie, egész nap szakaszosan adva.

Súlyos kiszáradás esetén bőr alatti vagy intravénás infúzióra lehet szükség, szükség esetén
akár többször is.

## Hasmenés {#hasmenes}

Sok FIP-es macskának hasmenése lesz. Megfelelő otthoni gyógymód lehet tökpürét és/vagy
probiotikumot adni, ezek jót tesznek és bármikor adhatók (hasmenéses állapot nélkül is).

A sütőtök püré csak sütőtököt tartalmazhat (nem pite töltelék!). A tökben lévő oldható rost
szabályozza a belek víztartalmát, és segít a hasmenés vagy a székrekedés gyógyításában is. Sok
macska kedveli az ízét, és készségesen megeszi nagyjából 3:1 táp/tök arányban.

Adható még Fortiflora probiotikum is.

*Lásd fentebb a kiszáradásról szóló részt is, mivel ez gyakran együtt jár a hasmenéssel.*

## Nehézlégzés (szörcsögés) {#nehezlegzes}

A nehézlégzés legszembetűnőbb tünete, hogy a légzés szapora és szabálytalan ütemű, valamint
szörcsögés kísérheti.

{% danger(title="Figyelem!") %}
Ha hirtelen tapasztalunk nehézlégzést, ne próbálkozzunk meg semmilyen házi kezeléssel. Ez
vészhelyzet lehet, amihez állatorvosi ellátás szükséges. Azonnal indulni kell, szükség esetén
ügyeletre is!
{% end %}

* Ha a nehézlégzés a szív vagy a tüdő körüli folyadék miatt következik be, akkor szükséges lehet a
  folyadék leszívása (ezt egyénileg kell eldönteni, és jobb minél inkább tartózkodni tőle, mivel a
  folyadék fehérjét és ásványi anyagokat tartalmaz, amire a szervezetnek szüksége lesz)

* Ha az ascites (a hasban lévő folyadék) nehézlégzést okoz, csak annyi folyadékot szívjunk le, amely
  a macska kényelmének megteremtéséhez szükséges. NE CSAPOLJUK LE AZ ÖSSZES FOLYADÉKOT!

Nehézlégzés esetén folyamatosan figyelemmel kell követnünk a cica légzésszámát. A gyógyulás folyamán
a folyadék felszívódásával ennek vissza kell állnia az egészséges tartományba.

{% note(title="Hogyan számoljunk légzést?") %}
Várjunk egy olyan pillanatra, amikor a cica mélyen alszik, lehetőleg az oldalán fekve. Vegyünk kézbe
egy stoppert, és indítsuk el a kilégzés legmélyebb pontján. Számoljuk a kilégzéseket egy percig.
Ha már egyenletes a légzés üteme, akkor elég lehet 15-30 másodpercig mérni, majd felszorozni a
kapott eredményt egy percnyire (pl. 30 másodperc esetén kettővel kell megszoroznunk a kapott
légzésszámot). Az egészséges nyugalmi légzésszám 15-30 közé tehető.
{% end %}


## Láz {#laz}

A macska normál testhőmérséklete 37,5°C - 39°C. Nagyon gyakori, hogy ez a nap folyamán ingadozik, ez
nem ok aggodalomra. Tartós láz vagy magas láz azonban állatorvosi vagy sürgősségi ellátást
igényelhet.

A láz nagyon gyakori a FIP-es macskáknál, ezért folyamatosan ellenőrizni kell őket. Lázas állapotban
a macska nem eszik, és általában csendesen vagy letargikusan viselkedik.

Ha a láz több mint 24 órán át fennáll, forduljunk állatorvoshoz.

## Étvágytalanság {#etvagytalansag}

Gyakran az egyik korai tünet, hogy a FIP-es cica nem akar magától enni. Döntő fontosságú, hogy
legalább 200 kalóriát kapjon naponta, különben a zsírmáj kialakulására nagy esély van és ez végzetes
is lehet.

Ajánlott az "A/D" típusú magas kalóriatartalmú állatorvosi táp. Ebben az étvágytalan időszakban
 kevésbé kell aggódni az ételek minősége miatt, sokkal fontosabb, hogy egyáltalán egyen.
Tehát bármivel lehet próbálkozni, amit egyébként egy cica ehet (például ízfokozós tápokat is
adhatunk)

* Amint a cica jobban érzi magát, mindenképpen térjünk vissza az egészséges ételekhez (ezzel
  kapcsolatban is tud felvilágosítást adni az állatorvos)

Arra is szükség lehet, hogy fecskendővel etessük a cicát (pl. Calopet pasztával). A B12 vitamin
étvágyfokozóként is működhet.

## Az injekció helyének sérülései {#inkekcios-serulesek}

A GS jelenlegi egyetlen ismert mellékhatása a vivőanyag savasságából fakad: a nagyszámú injekció
hatására szőrhullás, esetleg bőrprobléma léphet fel. Ez nem egy súlyos probléma, külsőleg kell
kezelni, és az injekciókat lehetőleg minél elszórtabban, más pontokon beadni napról-napra. A
szőrhullás kialakulása nem mindig elkerülhető, de csökkenthetjük a valószínűségét, ha mindig
alaposan letisztítjuk a bőrt az injekció előtt és után.

Ha előjön a probléma, vágjuk le a szőrzetet a sérült terület körül, tartsuk tisztán a területet, és
figyeljünk rá, hogy ne fertőződjön el a sérülés. Ha úgy tűnik, hogy fertőzött, beszéljünk
állatorvossal a további kezelésről (külsőleg vagy belsőleg, antibiotikummal kezelhető). Alkalmazható
Alphaderm vagy Cortizeme külsőleg.

## Hányinger {#hanyinger}

Szintén nagyon gyakori a FIP macskák körében, és gyakran az étvágytalanság mögöttes oka. A
hányinger tünetei közé tartozhat a nyáladzás, cuppogás, a túlzott rágás, vagy az étel iránti
érdeklődés, anélkül, hogy végül enne a cica.

Állatorvosi rendelvényre kaphatunk Cereniát, ami injekciós és tabletta formában egyaránt kapható.

Vörös szilfakéregpor adható kiegészítőként.

## Májfunkciók {#majfunkciok}

Nagyon gyakori a FIP következtében a májfunkciók csökkenése. A máj általában jól tud regenerálódni,
de ezt ajánlatos megtámogatni májvédő kiegészítőkkel. Magyarországon erre leggyakrabban Epato
pasztát vagy Hepa-Pet tablettát szoktak használni.


[1]: /gyogyitas