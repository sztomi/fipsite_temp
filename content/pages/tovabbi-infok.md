+++
title = "További információk"
path = "/tovabbi-infok"
template = "about.html"

[extra]
cover = "12341534_1671980023055733_133462647043205856_n"
subheading = "Források, videók, tanulmányok"
+++

Ezen az oldalon főként linkeket gyűjtünk a tudományos tudományos eredmények egy helyen gyűjtése
céljából. Ezenk kívül egyéb, témába vágó, hasznos cikkeket és videókat szeretnénk itt könnyen
megtalálhatóvá tenni.

## Tudományos anyagok

[A Veterinary Quarterly cikke FIP-pel kapcsolatos legújabb eredményekről][1]

[A Vet Microbiology-ban megjelent korai tanulmány a GS kezelésről][4]

[Dr. Pedersen 2019-es tanulmánya][5]

[A US National Public Library of Medicine cikke a FIP-ről][2]

[Modern diagnosztikai módszerek, immunfestés az MDPI Viruses folyóiratában][14]

[GS-441524 a wikipedián][7]

## Más, GS-kezeléssel foglalkozó oldalak

[fiptreatment.com][3] (a miénkhez hasonló oldal, angol nyelven)

[Dr. Addie][12]

## További kutatásokat támogató szervezetek

[sockfip.com][11] (Dr. Pedersen rendszeresen ír a kezelés körüli fejleményekről itt)

[ZenByCat][6]

[Bria Fund][8]

## Közösségek

[FIP Magyarország][10] (ide lépj be, ha szeretnéd elkezdeni a kezlést)

[FIP Warriors][9] (az eredeti nemzetközi csoport - jelenleg csak a kezelést újonnan kezdők számára
    nyitott)


## Videók

Bőr alá adott injekció beadását mutatja be Dr. Pedersen

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/okZ4V3JbCgo" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

A ZenByCat története

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/g1sSkqVchMk" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Interjú Dr. Pedersen-nel

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/8J6QRIGPzCY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<br>

## Egyéb cikkek

A [The Atlantic][13] cikke a GS-kezelésről és gyógyszer beszerzésének nehézségeiről

[1]: https://www.tandfonline.com/doi/full/10.1080/01652176.2020.1845917
[2]: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7150141/
[3]: https://fiptreatment.com/
[4]: https://pubmed.ncbi.nlm.nih.gov/29778200/
[5]: https://pubmed.ncbi.nlm.nih.gov/30755068/
[6]: https://www.zenbycat.org/fip
[7]: https://en.wikipedia.org/wiki/GS-441524
[8]: https://www.briafundsupporters.com/
[9]: https://www.facebook.com/groups/804374446995270/
[10]: https://www.facebook.com/groups/1059378271077244
[11]: https://sockfip.org
[12]: http://www.catvirus.com/treatment.htm
[13]: https://www.theatlantic.com/science/archive/2020/05/remdesivir-cats/611341/
[14]: https://www.mdpi.com/1999-4915/11/11/1068/pdf