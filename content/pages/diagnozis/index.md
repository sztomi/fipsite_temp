+++
title = "Diagnózis"
path = "diagnozis"
template = "about.html"

[extra]
cover = "yaopey-yong-DPJ4bp7keeg-unsplash"
subheading = "Hogyan diagnosztizálható a FIP?"
+++

*Szemben más betegségekkel (pl. FIV, FELV), a FIP megállapítására nincsen egyszerű teszt, amivel
minden kétséget kizáróan megállapítható lenne a betegség. Azonban a tünetek, valamint a vérlabor és
biokémiai eredmények együttes figyelembevételével mégis felállítható a diagnózis.*

A FIP tünetei kezdetben nagyon általánosak, és a betegség eléggé ritka ahhoz, hogy biztos diagnózis
hiányában az állatorvos más irányba tapogatózzon a kezelést illetően. Az itt ismertetett
diagnosztikai lépések segíthetnek abban, hogy gyorsabban sikerüljön felismerni a FIP-et.

FIP-es, vagy FIP-gyanús macska gazdájaként nagyon fontos, hogy felvértezd magad ezekkel az
információkkal, mert az időben elkezdett kezelés a tapasztalatok szerint kulcsfontosságú.

{% note(title="") %}
A legfontosabb <strong>objektív</strong> mutatók a teljes vérképben és biokémiában találhatók.
Elsősorban ezekre, valamint az ultrahangos vizsgálatra támaszkodjunk a diagnózis felállításakor.
{% end %}

## Nedves FIP

A nedves FIP legszembetűnőbb tünete a hasüregben felgyűlő, nagymennyiségű folyadék. Előfordulhat,
hogy a mellkasüregben keletkezik folyadék, ami kevésbé látványos, de nehézlégzést okozhat, ami egy
jóval veszélyesebb állapot. Ha a hasüregben keletkezik folyadék, a macska felfújtnak, körte
alakúnak fog látszódni, mint egy vízzel teli lufi.

{{ fluid_img(src="/diagnozis/nedves-fip-cilike" alt="Nedves FIP nagy mennyiségű hasüregi folyadékkal")}}

<p class="img-caption">Nedves FIP nagy mennyiségű hasüregi folyadékkal. <a href="/tulelok/cilike">Cilike</a> azóta gyógyult.</p>

További, általánosabb tünetek:

  * Étvágytalanság
  * Levertség (pl. elbújik)
  * Fáradékonyság (pl. kevesebbet, vagy kevésbé aktívan játszik)
  * Magas láz, ami nem reagál antibiotikumokra
  * Harmadik szemhéj kitüremkedése
  * Súlycsökkenés, fogyás
  * Sárgaság (fülek, orr, szájpadláson észrevehető)
  * Gyakran hasmenéses időszak a betegség kialakulása előtt

A hasüregi folyadék a tapasztalatlan gazdák számára könnyen összetéveszthető a puffadással vagy hízással,
de jellegzetes elhelyezkedésű, illetve az esetleges testsúlynövekedés mellett is csontosabbnak érződik a macska,
az izmaiból is fogyhat a betegség előrehaladásával.

A mellüregi folyadék esetében problémát jelezhet a megnövekedett légzésszám. A normális tartomány 15 és 30 között van,
ezt az állat nyugodt pillanataiban, ideális esetben alvás közben érdemes mérni. Légzés közben nedves,
szörcsögő hangot adhat a macska.

### Teljes vérkép és biokémia

Ha a FIP gyanúja felmerül a fentiek alapján, a következő lépés a teljes vérkép és biokémiai labor.
Egy tipikus FIP-es labor esetében:

  * Alacsony albumin
  * Magas globulin
  * Magas fehérvérsejtszám
  * Alacsony vörösvérsejtszám
  * Magas neutrofil
  * Magas fehérje
  * Magas bilirubin

A legtöbb FIP-es macska esetében nem-regeneratív vérszegénység és láz mérhető. **Nem minden vérkép
fog azonos mutatókat produkálni**, azonban ha néhány mutató jelen van, javasolt folytatni a
diagnosztikát.

### FIV/FELV tesztek

Mivel a FIV és FELV is gyakoribbak a FIP-nél, mindkettőt tesztelni kell, hogy kizárhatók legyenek. A
FIP és a két betegség tüneteinek egy része hasonló, ez pedig félrevezető lehet a diagnózis
felállításakor. Sajnos előfordulhat olyan eset is, hogy a fenti fertőzésekkel együtt is jelen van a
FIP.

### A folyadék vizsgálata

Egy fecskendővel mintát vesznek a folyadékból. A folyadék tipikus jellemzői:

  * Magas fehérjetartalom
  * Mézszínű, esetleg zöldes-sárgás árnyalatú
  * Sűrű, viszkózus

**RT-PCR a folyadékból**:

  * Ha a folyadék fenti jellemzői és a vérkép jellegzetes mutatói adottak, akkor nem érdemes
    PCR-tesztet csinálni (sőt, értékes időt veszíthetünk) [Bővebben erről itt.](/gyik#pcr)
  * Pozitív RT-PCR teszt igazolja koronavírus jelenlétét, de egyformán kimutatja a FIP vírust és a
  FeCV-t. Tehát önmagában ez nem döntő, de az egyéb tünetek és a folyadék jellemzői alapján után
  már az lehet.
  * Negatív RT-PCR teszt nem zárja ki a FIP-et (a hamis negatív esélye magas, 30% körüli).

{% note(title="Ne végezzünk RT-PCR tesztet a vérből vagy ürülékből") %}
Ezek gyakran lehetnek hamis pozitívak vagy hamis negatívak, tehát nem segítenek a bizonyosabb
diagnózis felállításában.
{% end %}

## Száraz FIP

A száraz FIP esetében a betegség lefolyása lassabb, a szervezet nem produkál olyan erőteljes
immunreakciót, így az észlelhető tünetek is sokkal általánosabbak, ami jelentősen nehezebbé teszi a
diagnózis felállítását. Ez a változat ritkább, mint a nedves FIP.

Az ebben a változatban szenvedő macskák gyakran mutatnak:

  * Étvágytalanság
  * Levertség (pl. elbújik)
  * Növekedésben visszamaradás, ha kölyök
  * Fáradékonyság (pl. kevesebbet, vagy kevésbé aktívan játszik)
  * Magas láz, ami nem reagál antibiotikumokra
  * Harmadik szemhéj kitüremkedése
  * Súlycsökkenés, fogyás
  * Gyakran hasmenéses időszak a betegség kialakulása előtt

## Idegrendszeri- és szemtünetek (neuro-FIP, okuláris FIP)

Ha a vírus átjut a vér-agy gáton, akkor neurológiai és- szemtünetek is kialakulhatnak. A szemtünetek
laikusként is észrevehetők olyan értelemben, hogy "valami nem stimmel" az macska szemével
(elszíneződés, homályosság). Az alábbi szembetegségek alakulhatnak ki:

  * Belső szemgyulladás (uveitis)
  * Külső szemgyulladás (Szaruhártya precipitátumok, Keratic precipitates)
  * Csarnokvíz zavarossága (aqueous flare)
  * Retina vasculitis (erek gyulladása)

Neurológiai tünetek:

  * Ataxia (részleges vagy teljes bénulás, elsőként a hátsó lábakon)
  * Remegés
  * Rohamok
  * Nystagmus (szemek nem szándékolt mozgatása, remegése)

{% note(title="Neurológiai és okuláris tünetek előfordulása") %}
A neurológiai és okuláris tünetek mind a száraz, mind a nedves FIP esetében előfordulhatnak.
{% end %}

### Teljes vérkép és biokémia

Ha a FIP gyanúja felmerül a fentiek alapján, a következő lépés a teljes vérkép és biokémiai labor.
Egy tipikus FIP-es labor esetében:

  * Alacsony albumin
  * Magas globulin
  * Magas fehérvérsejtszám
  * Alacsony vörösvérsejtszám
  * Magas neutrofil
  * Magas fehérje
  * Magas bilirubin

A legtöbb FIP-es macska esetében nem-regeneratív vérszegénység és láz mérhető. Nem minden vérkép
fog azonos mutatókat produkálni, azonban ha néhány mutató jelen van, javasolt folytatni a
diagnosztikát.

### Ultrahang

A száraz FIP elváltozásokat okoz az érintett szervekben, amiket ultrahangos vizsgálattal fel lehet
fedezni. További jel lehet a mesentriális nyirokcsomók és a vesék megnagyobbodása. A vizsgálat
közben az elváltozást mutató területekről szövetminta vehető, amelyet immunfestéses vizsgálatra
lehet küldeni.

### Immunfestés

Az FCoV vírusok megfestése [direkt immunofluoreszcens][1] eljárással a cytocentrifugált hasi
folyadékban vagy immunohisztokémiai megfestése szövetmintáknak a legbiztosabb módja a vírusok
kimutatásának. Az immunfestés sem tud különbséget tenni az ártalmatlan FCov és a FIP-et kiváltó FCov
között, de krónikus gyulladásos szövetben talált fertőzött makrofágok már erős bizonyítékot
jelentenek. Egy friss [tanulmányban][2] vizsgálva az immunfestéses módszer 97-100%-ban prediktálta a
FIP fertőzést.

Bár az immunfestés nagy hatékonysággal azonosítja a jelenlévő FCov vírusokat, a negatív eredmény már
sokkal kisebb eséllyel döntő. Ez azt jelenti, hogy egy pozitív immunfestéses teszt nagy
valószínűséggel alátámasztja a diagnózist, viszont a negatív teszt még nem fogja kizárni.

### GS, mint diagnosztikai eszköz

Ha az egyéb tünetek önmagukban még nem elengendőek a diagnózis felállításához, a GS kezelés
akkor is elkezdhető, mert rendszerint gyors javulást hoz (és kevés ismert mellékhatása van). Ha
bekövetkezik a javulás, akkor a diagnózis biztossá válik és a kezelés folytatható.

{% warning(title="FIGYELEM: nem létezik FIP-teszt") %}
A koronavírusok kimutatására alkalmas PCR-tesztre néha
(hibásan) FIP-tesztként hivatkoznak. Ez egy félreértés: a PCR teszt mind az ártalmatlan FeCV-t,
mind a betegséget okozó FIPV-t kimutatja és nem tesz közöttük különbséget. Tehát önmagában ez a
teszt nem fogja sem kizárni, sem bizonyítani a FIP diagnózist. Ugyanez igaz az antitesteket mérő
ELISA tesztre is. Mindkét teszt csak az egyéb vizsgálati eredményekkel együtt értelmezhető.
{% end %}


[1]: https://hu.wikipedia.org/wiki/Immunfluoreszcencia#Direkt_immunfluoreszcencia
[2]: https://www.mdpi.com/1999-4915/11/11/1068/pdf